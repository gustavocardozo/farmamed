package comunication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import comunication.PharmacyGateway;
import model.*;

public class PharmacyGatewayTest {
	private PharmacyGateway gateway;
	
	private Remedy remedyExist;
	private Remedy remedyNotExist;
	private List<Pharmacy> pharmaciesExist;
	private List<Pharmacy> pharmaciesNotExist;
	
	private String URL_PHARMACY_EMPTY = "http://www.mocky.io/v2/5ccc2ebc3300000133e01b50";
	private String URL_PHARMACY_ERROR = "http://www.mocky.io/v2/5ccc34ef3300000133e01b9e";
	private String URL_PHARMACY_100 = "http://www.mocky.io/v2/5ccc369d3300000133e01bab";
	private String URL_PHARMACY_UNKNOWN = "http://www.mocky.io/v2/5ccc3a25330000293fe01bc6";
	
	private String METHOD = "GET";
	
	@Before
	public void instantiateObjects() {
		gateway = instanceGateway();
		remedyExist = new Remedy("Ibuprofeno", "", "");
		remedyNotExist = new Remedy("Amoxilina", "", "");
		
		pharmaciesExist = new ArrayList<Pharmacy>();
		pharmaciesNotExist = new ArrayList<Pharmacy>();
		Address dir1 = new Address("Pres. Rivadavia 1600 Jos� C. Paz", 0, "");
		pharmaciesExist.add(new Pharmacy("Farmacia del Aguila", dir1, "02320 649617"));
		Address dir2 = new Address("Pres. Rivadavia 8500 San Miguel", 0, "");
		pharmaciesExist.add(new Pharmacy("Farmacia Per�n", dir2, "02320 665522"));
	}
	
	private PharmacyGateway instanceGateway() {
		return new PharmacyGateway();
	}
	
	/* CA 1 */
	@Test
	public void testNotEmptyPharmacies() {
		List<Pharmacy> resu = gateway.getByRemedy(remedyExist);
		assertEquals(pharmaciesExist.get(0).getName(),resu.get(0).getName());
		assertEquals(pharmaciesExist.get(1).getTel(),resu.get(1).getTel());
	}
	
	/* CA 2 */
	@Test
	public void testEmptyPharmacies() {
		PharmacyGateway gate = new PharmacyGateway(URL_PHARMACY_EMPTY, METHOD);
		assertEquals(pharmaciesNotExist,gate.getByRemedy(remedyNotExist));
	}
	
	/* CA 3 */
	@Test
	public void testEmptyRemedy() {
		PharmacyGateway gate = new PharmacyGateway(URL_PHARMACY_EMPTY, METHOD);
		assertEquals(pharmaciesNotExist,gate.getByRemedy(new Remedy("","","")));
	}
	
	/* CA 4 */
	@Test
	public void testErrorPrueba() {
		PharmacyGateway gate = new PharmacyGateway(URL_PHARMACY_ERROR, METHOD);
		assertEquals(pharmaciesNotExist,gate.getByRemedy(remedyExist));
	}
	
	/* CA 5*/
	@Test
	public void testPharmacy100() {
		PharmacyGateway gate = new PharmacyGateway(URL_PHARMACY_100, METHOD);
		List<Pharmacy> resu = gate.getByRemedy(new Remedy("Medicamento X", "", ""));
		assertEquals("Farmacia 100",resu.get(0).getName());
	}
	
	/* CA 6*/
	@Test
	public void testPharmacyUnknown() {
		PharmacyGateway gate = new PharmacyGateway(URL_PHARMACY_UNKNOWN, METHOD);
		gate.getByRemedy(new Remedy("Medicamento K", "", ""));
		assertEquals("Medicamento desconocido",gate.getError());
	}
	
	/* CA 7*/
	@Test
	public void testURLFake() {
		PharmacyGateway gate = new PharmacyGateway("xxxxxxxxx", METHOD);
		gate.getByRemedy(new Remedy("Medicamento K", "", ""));
		assertTrue(gate.getError() != "");
	}
}
