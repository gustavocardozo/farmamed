package comunication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import model.Address;
import model.Pharmacy;
import model.Remedy;
import model.RemedyArticle;
import model.User;
import validators.BuyRemedyValidator;

public class MercadopagoGatewayTest {
	private String ACCESS_TOKEN = "TEST-7320367974254619-050509-a1598e7614598d7423c793cc4414c3b9__LC_LD__-37675140";
	private MercadopagoGateway gateway;
	private HashMap<RemedyArticle, Pharmacy> stage;
	private User loggedUser;
	private Pharmacy cientotres;
	private Pharmacy cientocinco;
	private Remedy o;
	private Remedy h;
	private RemedyArticle articleO;
	private RemedyArticle articleH;
	
	private List<RemedyArticle> availableArticles = new ArrayList<RemedyArticle>();
	private List<MPPaymentMethodDTO> cashMethods = new ArrayList<MPPaymentMethodDTO>();
	
	private BuyRemedyValidator purchasingValidator;
	
	private HashMap<RemedyArticle, Pharmacy> buildStage() {
		
		// Medicamentos
		o = new Remedy("Drug 0", "", "");
		h = new Remedy("Drug H", "", "");
		
		// Farmacias
		Address address = new Address("Marcos Sastre 2215", 521, "Ricardo Rojas");
		cientotres = new Pharmacy("Farmacia 103", address, "521");
		cientocinco = new Pharmacy("Farmacia 105", null, "");
		
		// Articles
		articleO = new RemedyArticle(o, Float.parseFloat("100"));
		articleH = new RemedyArticle(h, Float.parseFloat("250"));
		availableArticles.add(articleO);
		availableArticles.add(articleH);
		
		HashMap<RemedyArticle, Pharmacy> list = new HashMap<RemedyArticle, Pharmacy>();
		list.put(articleO, cientotres);
		list.put(articleH, cientotres);
		
		return list;
	}
	
	private List<MPPaymentMethodDTO> getCashMethods() {
		return gateway.getPaymentMethodCash();
	}
	
	@Before
	public void instantiateObjects() {
		gateway = new MercadopagoGateway(ACCESS_TOKEN);
		stage = buildStage();
		purchasingValidator = new BuyRemedyValidator(stage);
		cashMethods = getCashMethods();
		loggedUser = new User("Gustavo", "Cardozo", 1551515616, false, false);
		loggedUser.setEmail("gustavo.cdz@gmail.com");
	}
	
	/* CA 1 */
	@Test
	public void testNoValidArticle() {
		RemedyArticle article = new RemedyArticle(new Remedy("Drug K", "", ""), 100);
		assertFalse(purchasingValidator.validate(article, cientotres));
	}
	
	@Test
	public void testValidArticle() {
		assertTrue(purchasingValidator.validate(articleO, cientotres));
	}
	
	/* CA 2 */
	@Test
	public void testChangePriceArticle() {
		RemedyArticle aux = new RemedyArticle(articleO.getRemedy(), 5000);
		assertFalse(purchasingValidator.validate(aux, cientotres));
	}
	
	/* CA 3 */
	@Test
	public void testInvalidBuy() {
		assertTrue(purchasingValidator.validate(articleO, cientotres));
		assertNull(gateway.buyRemedy(loggedUser, articleO, new MPPaymentMethodDTO("xxxxx", "xxxxx", "xxxxxx")));
	}
	
	/* CA 4 */
	@Test
	public void testValidBuy() {
		assertTrue(purchasingValidator.validate(articleO, cientotres));
		MPPaymentDTO payment = gateway.buyRemedy(loggedUser, articleO, cashMethods.get(0));
		assertNotNull(payment);
		// Mostrar que el pago se ejecutó
        System.out.println(payment.getExternalResourceUrl());
	}
	
	/* CA 5 */
	@Test
	public void testWithOutPrice() {
		RemedyArticle test = new RemedyArticle(new Remedy("XXXXX", "", ""), 0);
		assertFalse(purchasingValidator.validate(test, cientotres));
	}
}
