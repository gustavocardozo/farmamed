package comunication;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PamiCodeCircuitTest {
	private String URL_PAMI_OK = "http://www.mocky.io/v2/5cb54968330000da0c5d7a18";
	private String URL_PAMI_FAIL = "http://www.mocky.io/v2/5cb54968330000da0c5d7a18?mocky-delay=4s";
	private String METHOD = "GET";
	
	private PamiCodeCircuit circuit;
	private PamiCodeSupplier service;
	
	public PamiCodeCircuit instanceFakeService() {
		PamiCodeSupplier service = new PamiCodeSupplier(URL_PAMI_FAIL, METHOD);
		PamiCodeCircuit circuit = new PamiCodeCircuit(service, PamiCodeCircuit.Status.CLOSED);
		return circuit;
	}
	
	public PamiCodeCircuit instanceOpenCircuit() {
		PamiCodeCircuit fakeCircuit = instanceFakeService();
		assertEquals(false, fakeCircuit.validate("1234"));
		assertEquals(false, fakeCircuit.validate("1234"));// It's Open
		return fakeCircuit;
	}

	@Before
	public void instantiateObjects() {
		service = new PamiCodeSupplier(URL_PAMI_OK, METHOD);
		circuit = new PamiCodeCircuit(service, PamiCodeCircuit.Status.CLOSED);
	}
	
	
	@Test
	public void testCodeOk() {
		assertEquals(true, circuit.validate("1234"));
	}
	
	@Test
	public void testCodeError() {
		assertEquals(false, instanceFakeService().validate("1234"));
	}
	
	@Test
	public void testClosedCircuit() {
		assertEquals(true, circuit.validate("1234"));
		assertEquals(true, circuit.validate("1234"));
		assertEquals(PamiCodeCircuit.Status.CLOSED, circuit.getStatus());
	}
	
	/* CA 5*/
	@Test
	public void testOpenCircuit() {
		PamiCodeCircuit fakeCircuit = instanceFakeService();
		assertEquals(false, fakeCircuit.validate("1234"));
		assertEquals(false, fakeCircuit.validate("1234"));
		assertEquals(PamiCodeCircuit.Status.OPEN, fakeCircuit.getStatus());
	}
	
	/* CA 6 */
	@Test
	public void testCloseOpenCloseCircuit() {
		PamiCodeCircuit fakeCircuit = instanceFakeService();
		assertEquals(false, fakeCircuit.validate("1234"));
		assertEquals(false, fakeCircuit.validate("1234"));// It's Open
		assertEquals(false, fakeCircuit.validate("1234"));// AutoResponse
		assertEquals(false, fakeCircuit.validate("1234"));// AutoResponse
		assertEquals(PamiCodeCircuit.Status.CLOSED, fakeCircuit.getStatus());
	}
	
	/* CA 3 */
	@Test
	public void testCircuitOpenFirst() {
		PamiCodeCircuit fakeCircuit = instanceOpenCircuit();
		assertEquals(false, fakeCircuit.validate("1234"));// AutoResponse
	}
	
	/* CA 4 */
	@Test
	public void testCircuitOpenSecond() {
		PamiCodeCircuit fakeCircuit = instanceOpenCircuit();
		assertEquals(false, fakeCircuit.validate("9876"));// AutoResponse
	}
}
