package comunication;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;


public class PamiCodeGatewayTest {
	private String URL_PAMI_OK = "http://www.mocky.io/v2/5cb54968330000da0c5d7a18";
	private String URL_PAMI_FAIL = "http://www.mocky.io/v2/5cb54968330000da0c5d7a18?mocky-delay=4s";
	private String METHOD = "GET";
	
	private PamiCodeCircuit circuit;
	private PamiCodeSupplier service;
	private PamiCodeGateway gateway;

	public PamiCodeGateway instanceFakeService() {
		PamiCodeSupplier service = new PamiCodeSupplier(URL_PAMI_FAIL, METHOD);
		PamiCodeCircuit circuit = new PamiCodeCircuit(service, PamiCodeCircuit.Status.CLOSED);
		PamiCodeGateway gateway = new PamiCodeGateway(circuit);
		return gateway;
	}
	
	@Before
	public void instantiateObjects() {
		service = new PamiCodeSupplier(URL_PAMI_OK, METHOD);
		circuit = new PamiCodeCircuit(service, PamiCodeCircuit.Status.CLOSED);
		gateway = new PamiCodeGateway(circuit);
	}
	
	/* CA 1 */
	@Test
	public void testCodeOk() {
		assertEquals(true, gateway.validate("1234"));
	}
	
	/* CA 2 */
	@Test
	public void testCodeEror() {
		assertEquals(false, instanceFakeService().validate("1234"));
	}
}
