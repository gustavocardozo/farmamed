package ObjectTest;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import bussiness.NotificationManager;
import model.Notification;
import model.User;

public class NotifierObjectTest implements Observer{

	private Notification notification;
	private NotificationManager manager;
	private ArrayList<Notification> notificationsSent = new ArrayList<Notification>();
	
	@Override
	public void update(Observable o, Object notification) {
		if(notification != null) {
			this.notification = (Notification) notification;
			notificationsSent.add(this.notification);
			try {
				manager.notificationReceived(this.notification.getStatus());
			} catch (Exception e) {
			}
		}
	}
	
	public Notification getN() {
		return notification;
	}
	
	public void setNotificationManager(NotificationManager manager) {
		this.manager = manager;
	}
	
	public ArrayList<Notification> getNotificationsSent(){
		return notificationsSent;
	}
	
	public void simulateReading(User user, int index) {
		Notification n = user.getNotifications().get(index);
		manager.notificationRead(n.getStatus());
		manager.notificationClean(user, n);
	}

}
