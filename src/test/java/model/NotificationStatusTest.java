package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Before;
import org.junit.Test;

public class NotificationStatusTest {
	
	private NotificationStatus status;
	
	@Before
	public void instantiateObjects() {
		status = new NotificationStatus();
	}
	
	@Test
	public void testIsReceived() {
		assertEquals(false, status.isReceived());
		assertNotEquals(true, status.isReceived());
	}
	
	@Test
	public void testIsRead() {
		assertEquals(false, status.isRead());
		assertNotEquals(true, status.isRead());
	}
	
	@Test
	public void testSetReceived() {
		assertEquals(false, status.isReceived());
		status.setReceived(true);
		assertNotEquals(false, status.isReceived());
	}
	
	@Test
	public void testSetRead() {
		assertEquals(false, status.isRead());
		status.setRead(true);
		assertNotEquals(false, status.isRead());
	}
}
