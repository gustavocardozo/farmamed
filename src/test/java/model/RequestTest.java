package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class RequestTest {

	private Request request;
	private User user, userAux;
	private Recipe recipe, recipeAux;
	private LocalDate date, dateAux;
	
	@Before
	public void instantiateObjects() {
		user = new User("UserNameA", "UserSurnameA", 12, false, false);
		userAux = new User("UserNameB", "UserSurnameB", 99, false, false);
		recipe = new Recipe(987);
		recipeAux = new Recipe(561);
		request = new Request(user, recipe);
		date = request.getDate();
		dateAux = LocalDate.of(1990, 9, 9);
	}
	
	@Test
	public void testGetUser() {
		assertEquals(user, request.getUser());
		assertNotEquals(userAux, request.getUser());
	}
	
	@Test
	public void testGetRecipe() {
		assertEquals(recipe, request.getRecipe());
		assertNotEquals(recipeAux, request.getRecipe());
	}
	
	@Test
	public void testGetDate() {
		assertEquals(date, request.getDate());
		assertNotEquals(dateAux, request.getDate());
	}
	
	@Test
	public void testGetStatus() {
		assertNotNull(request.getStatus());
	}
	
	@Test
	public void testGetPriority() {
		assertEquals(0, request.getPriority());
		request.setPriority(2);
		assertEquals(2, request.getPriority());
		assertNotEquals(0, request.getPriority());
	}
	
	@Test
	public void testSetUser() {
		assertEquals(user, request.getUser());
		request.setUser(userAux);
		assertNotEquals(user, request.getUser());
	}
	
	@Test
	public void testSetRecipe() {
		assertEquals(recipe, request.getRecipe());
		request.setRecipe(recipeAux);
		assertNotEquals(recipe, request.getRecipe());
	}
	
}
