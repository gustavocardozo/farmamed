package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

public class PharmacyTest {

	private Pharmacy pharmacy;
	private Address address;
	
	@Before
	public void instantiateObjects() {
		address = new Address("Calle 1", 987, "Ciudad 1");
		pharmacy = new Pharmacy("Farmacia A", address, "5678");
	}
	
	@Test
	public void testGetName() {
		assertEquals("Farmacia A", pharmacy.getName());
		assertNotEquals("Farmacia B", pharmacy.getName());
	}
	
	@Test
	public void testGetAddress() {
		assertEquals(address, pharmacy.getAddress());
		Address addressAux = new Address("Calle 2", 987, "Ciudad 2");
		assertNotEquals(addressAux, pharmacy.getAddress());
	}
	
	@Test
	public void testGetTel() {
		assertEquals("5678", pharmacy.getTel());
		assertNotEquals("9876", pharmacy.getTel());
	}
	
	@Test
	public void testSetName() {
		assertEquals("Farmacia A", pharmacy.getName());
		pharmacy.setName("Farmacia B");
		assertNotEquals("Farmacia A", pharmacy.getName());
	}
	
	@Test
	public void testSetAddress() {
		assertEquals(address, pharmacy.getAddress());
		Address addressAux = new Address("Calle 2", 987, "Ciudad 2");
		pharmacy.setAddress(addressAux);
		assertNotEquals(address, pharmacy.getAddress());
	}
	
	@Test
	public void testSetTel() {
		assertEquals("5678", pharmacy.getTel());
		pharmacy.setTel("9876");
		assertNotEquals("5678", pharmacy.getTel());
	}
}
