package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import ObjectTest.NotifierObjectTest;
import bussiness.NotificationManager;
import bussiness.Notifier;

public class SubjectObservableTest {
	
	private NotifierObjectTest observer;
	private SubjectObservable observable;
	
	@Before
	public void instantiateObjects() {
		observer = new NotifierObjectTest();
		observable = new Notifier(new NotificationManager());
	}
	
	@Test
	public void testAddObserver() {
		assertTrue(observable.observers.isEmpty());
		observable.addObserver(observer);
		assertTrue(observable.observers.size() == 1);
	}
	
	@Test
	public void testAddObserver2() {
		assertEquals(0,observable.countObservers());
		observable.addObserver(observer);
		assertEquals(1, observable.countObservers());
	}
	
	@Test
	public void testCountObservers() {
		assertEquals(0, observable.countObservers());
		observable.addObserver(observer);
		assertEquals(1, observable.countObservers());
		assertNotEquals(0, observable.countObservers());
	}
	
	@Test
	public void testDeleteObserver() {
		observable.addObserver(observer);
		assertEquals(1, observable.countObservers());
		observable.deleteObserver(observer);
		assertEquals(0, observable.countObservers());
		assertNotEquals(1, observable.countObservers());
	}
	
	@Test
	public void testDeleteObservers() {
		observable.addObserver(observer);
		assertEquals(1, observable.countObservers());
		observable.deleteObservers();
		assertEquals(0, observable.countObservers());
		assertNotEquals(1, observable.countObservers());
	}
	
	@Test
	public void notifyObservers() {
		observable.addObserver(observer);
		observable.setChanged();
		assertTrue(observable.hasChanged());
		observable.notifyObservers();
		assertFalse(observable.hasChanged());
		assertNull(observer.getN());
	}
	
	@Test
	public void notifyObservers2() {
		observable.addObserver(observer);
		observable.setChanged();
		assertTrue(observable.hasChanged());
		assertNull(observer.getN());
		observable.notifyObservers(new Notification("N1"));
		assertFalse(observable.hasChanged());
		assertEquals("N1", observer.getN().getNotificationMessage());
	}

}
