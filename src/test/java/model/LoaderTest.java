package model;

import comunication.PharmacyAddressGateway;
import org.junit.Before;
import org.junit.Test;
import comunication.LoaderGateway;

import java.util.List;

import static org.junit.Assert.*;

public class LoaderTest {
    private List<LoaderGateway> relations;


    public static String URL_PHARMACIES = "http://www.mocky.io/v2/5cf08cb53000004b0000b974";
    public static String METHOD = "GET";

    private LoaderGateway<Pharmacy, Address> addressSource;
    Pharmacy cientotres;
    Pharmacy cientocinco;

    @Before
    public void instantiateObjects() {
        addressSource = new PharmacyAddressGateway(URL_PHARMACIES, METHOD);
        cientotres = new Pharmacy(1, addressSource, "Farmacia 103", null, "02320649617" );
        cientocinco = new Pharmacy(2, addressSource, "Farmacia 105", null, "1166958722" );
    }

    // CA1
    @Test
    public void testLoad() {
        Address direccion = cientotres.getAddress();
        assertEquals(direccion.getNumber(), 521);
        assertTrue(cientotres.getLoadFlag());
    }

    // CA2
    @Test
    public void testNoAddress() {
        Address direccion = cientocinco.getAddress();
        assertNull(direccion);
    }

    // CA3
    @Test
    public void testChangeAddress() {
        cientocinco.setAddress(cientotres.getAddress());
        assertFalse(cientocinco.getLoadFlag());
    }

    // CA4
    @Test
    public void testChangeNullAddress() {
        cientocinco.setAddress(null);
        assertNull(cientocinco.getAddress());
        assertFalse(cientocinco.getLoadFlag());
    }

    // CA5
    @Test
    public void testChangeRealAddress() {
        Address aux = cientotres.getAddress();
        aux.setNumber(200);
        assertEquals(cientotres.getAddress().getNumber(), 200);
    }
}
