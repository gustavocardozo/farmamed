package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class NotificationTest {
	
	private Notification notification;
	
	@Before
	public void instantiateObjects() {
		notification = new Notification("notify N");
	}
	
	@Test
	public void testGetNotificationMessage() {
		assertEquals("notify N", notification.getNotificationMessage());
		assertNotEquals("notify V", notification.getNotificationMessage());
	}
	
	@Test
	public void testGetStatus() {
		assertNotNull(notification.getStatus());
	}
	
	@Test
	public void testSetNotificationMessage() {
		assertEquals("notify N", notification.getNotificationMessage());
		notification.setNotificationMessage("notify R");
		assertNotEquals("notify N", notification.getNotificationMessage());
	}

}
