package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

public class RecipeTest {

	private Recipe recipe;
	
	@Before
	public void instantiateObjects() {
		recipe = new Recipe(987);
	}
	
	@Test
	public void testGetRecipeNumber() {
		assertEquals(987, recipe.getRecipeNumber());
		assertNotEquals(125, recipe.getRecipeNumber());
	}
	
	@Test
	public void testSetRecipeNumber() {
		assertEquals(987, recipe.getRecipeNumber());
		recipe.setRecipeNumber(125);
		assertNotEquals(987, recipe.getRecipeNumber());
	}
}
