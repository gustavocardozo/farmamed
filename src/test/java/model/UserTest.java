package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class UserTest {
	
	
	private User user;
	
	@Before
	public void instantiateObjects() {
		user = new User("UserNameA", "UserSurnameA", 12, false, false);
	}
	
	@Test
	public void testGetName() {
		assertEquals("UserNameA", user.getName());
		assertNotEquals("UserNameB", user.getName());
	}
	
	@Test
	public void testGetSurname() {
		assertEquals("UserSurnameA", user.getSurname());
		assertNotEquals("UserSurnameB", user.getSurname());
	}
	
	@Test
	public void testGetAffiliateNumber() {
		assertEquals(12, user.getAffiliateNumber());
		assertNotEquals(11, user.getAffiliateNumber());
	}
	
	@Test
	public void testSetName() {
		assertEquals("UserNameA", user.getName());
		user.setName("UserNameB");
		assertNotEquals("UserNameA", user.getName());
	}
	
	@Test
	public void testSetSurname() {
		assertEquals("UserSurnameA", user.getSurname());
		user.setSurname("UserSurnameB");
		assertNotEquals("UserSurnameA", user.getSurname());
	}
	
	@Test
	public void testSetAffiliateNumber() {
		assertEquals(12, user.getAffiliateNumber());
		user.setAffiliateNumber(01);
		assertNotEquals(12, user.getAffiliateNumber());
	}
	
	@Test
	public void testGetNotifications() {
		assertNotNull(user.getNotifications());
	}
	
	@Test
	public void testIsDiabeticAndSet() {
		assertFalse(user.isDiabetic());
		user.setDiabetic(true);
		assertFalse(!user.isDiabetic());
		assertTrue(user.isDiabetic());
		user.setDiabetic(false);
		assertFalse(user.isDiabetic());
		assertTrue(!user.isDiabetic());
	}

	@Test
	public void testIsOncologicalAndSet() {
		assertFalse(user.isOncological());
		user.setOncological(true);
		assertFalse(!user.isOncological());
		assertTrue(user.isOncological());
		user.setOncological(false);
		assertFalse(user.isOncological());
		assertTrue(!user.isOncological());
	}
}
