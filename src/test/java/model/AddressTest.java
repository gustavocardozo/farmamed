package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

public class AddressTest {

	private Address address;
	
	@Before
	public void instantiateObjects() {
		address = new Address("Calle 1", 987, "Ciudad A");
	}
	
	@Test
	public void testGetStreet() {
		assertEquals("Calle 1", address.getStreet());
		assertNotEquals("Calle 2", address.getStreet());
	}
	
	@Test
	public void testGetNumber() {
		assertEquals(987, address.getNumber());
		assertNotEquals(125, address.getNumber());
	}
	
	@Test
	public void testGetCity() {
		assertEquals("Ciudad A", address.getCity());
		assertNotEquals("Ciudad B", address.getCity());
	}
	
	@Test
	public void testSetStreet() {
		assertEquals("Calle 1", address.getStreet());
		address.setStreet("Calle 2");
		assertNotEquals("Calle 1", address.getStreet());
	}
	
	@Test
	public void testSetNumber() {
		assertEquals(987, address.getNumber());
		address.setNumber(125);
		assertNotEquals(987, address.getNumber());
	}
	
	@Test
	public void testSetCity() {
		assertEquals("Ciudad A", address.getCity());
		address.setCity("Ciudad B");
		assertNotEquals("Ciudad A", address.getCity());
	}
}
