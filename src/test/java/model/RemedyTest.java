package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

public class RemedyTest {

	private Remedy remedy;
	
	@Before
	public void instantiateObjects() {
		remedy = new Remedy("Droga R", "Marca R", "10 mg");
	}
	
	@Test
	public void testGetDrug() {
		assertEquals("Droga R", remedy.getDrug());
		assertNotEquals("Droga M", remedy.getDrug());
	}
	
	@Test
	public void testGetTrademark() {
		assertEquals("Marca R", remedy.getTrademark());
		assertNotEquals("Marca M", remedy.getTrademark());
	}
	
	@Test
	public void testGetVersion() {
		assertEquals("10 mg", remedy.getVersion());
		assertNotEquals("20 mg", remedy.getVersion());
	}
	
	@Test
	public void testSetDrug() {
		assertEquals("Droga R", remedy.getDrug());
		remedy.setDrug("Droga M");
		assertNotEquals("Droga R", remedy.getDrug());
	}
	
	@Test
	public void testSetTrademark() {
		assertEquals("Marca R", remedy.getTrademark());
		remedy.setTrademark("Marca M");
		assertNotEquals("Marca R", remedy.getTrademark());
	}
	
	@Test
	public void testSetVersion() {
		assertEquals("10 mg", remedy.getVersion());
		remedy.setVersion("2o mg");
		assertNotEquals("10 mg", remedy.getVersion());
	}

	
}
