package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Before;
import org.junit.Test;

public class RequestStatusTest {

	private RequestStatus status;
	
	@Before
	public void instantiateObjects() {
		status = new RequestStatus();
	}
	
	@Test
	public void testIsWaiting() {
		assertEquals(false, status.isWaiting());
		assertNotEquals(true, status.isWaiting());
	}
	
	@Test
	public void testSetWaiting() {
		assertEquals(false, status.isWaiting());
		status.setWaiting(true);
		assertNotEquals(false, status.isWaiting());
	}
	
	@Test
	public void testisSearchPerformed() {
		boolean searchPerformed = status.isSearchPerformed();
		assertEquals(false, searchPerformed);
		assertNotEquals(true, status.isSearchPerformed());
		status.setSearchPerformed(true);
		searchPerformed = status.isSearchPerformed();
		assertEquals(true, searchPerformed);
		assertNotEquals(false, searchPerformed);
	}
	
	@Test
	public void testSetSearchPerformed() {
		assertEquals(false, status.isSearchPerformed());
		status.setSearchPerformed(true);
		assertNotEquals(false, status.isSearchPerformed());
	}
	
	@Test
	public void testisSuccessfulSearch() {
		assertEquals(false, status.isSuccessfulSearch());
		assertNotEquals(true, status.isSuccessfulSearch());
	}
	
	@Test
	public void testSetSuccessfulSearch() {
		assertEquals(false, status.isSuccessfulSearch());
		status.setSuccessfulSearch(true);
		assertNotEquals(false, status.isSuccessfulSearch());
	}
}
