package validators;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import bussiness.ConnectionManager;
import comunication.PharmacyGateway;

public class PharmacyGatewayValidatorTest {
	protected PharmacyGatewayValidator validator;
	protected ConnectionManager connection;
	
	@Before
	public void instantiateObjects() {
		validator = new PharmacyGatewayValidator();
		connection = new ConnectionManager();
	}

	@Test
	public void testFormatValid() {
		assertTrue(validator.validate(connection.call(PharmacyGateway.URL_MEDICAMENTS, PharmacyGateway.METHOD)));
	}
	
	@Test
	public void testFormatInvalid() {
		assertFalse(validator.validate("ERRORPRUEBA"));
	}
}
