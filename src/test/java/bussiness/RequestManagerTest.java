package bussiness;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import model.Recipe;
import model.Request;
import model.User;

public class RequestManagerTest {

	private RequestManager manager;
	private User u1, u2, u3, u4;
	
	@Before
	public void instantiateObjects() {
		manager = new RequestManager();
		u1 = new User("User 1", "TestUser 1", 1234, false, true);
		u2 = new User("User 2", "TestUser 2", 1234, false, false);
		u3 = new User("User 3", "TestUser 3", 1234, true, true);
		u4 = new User("User 4", "TestUser 4", 1234, true, false);
	}
	
	@Test
	public void testCreateRequest() {
		Request request = null; 
		assertNull(request);
		request = manager.createRequest(u1, null);
		assertEquals("User 1", request.getUser().getName());
	}
	
	@Test
	public void testAddWaitingRequest() {
		assertEquals(0, manager.getWaitingRequests().size());
		manager.addWaitingRequest(manager.createRequest(u1, null));
		assertEquals(1, manager.getWaitingRequests().size());
		assertNotEquals(0, manager.getWaitingRequests().size());
	}

	@Test
	public void testGetRequestStateModifier() {
		RequestStateModifier stateModifier = null;
		assertNull(stateModifier);
		stateModifier = manager.getRequestStateModifier();
		assertNotNull(stateModifier);
	}
}
