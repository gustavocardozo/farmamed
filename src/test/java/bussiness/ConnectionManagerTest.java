package bussiness;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.Notification;
import model.User;

public class ConnectionManagerTest {
	private static String URL_NUMBER = "http://www.mocky.io/v2/5cca5758310000bf4912ceb4";
	private static String URL_BOOL_TRUE = "http://www.mocky.io/v2/5cca5791310000614612ceb5";
	private static String URL_ERROR_400 = "http://www.mocky.io/v2/5cca59cd310000bf4912ceba";
	private static String URL_STRING = "http://www.mocky.io/v2/5cca5a0f310000bf4912cebb";
	private ConnectionManager connection;

	@Before
	public void instantiateObjects() {
		connection = new ConnectionManager();
	}
	
	@Test
	public void testFakeUrl() {
		assertEquals("",connection.call("xxxxxxxx", "GET"));
	}
	
	@Test
	public void testFakeMethod() {
		assertEquals("",connection.call("xxxxxxxx", "XX"));
	}
	
	@Test
	public void testNullUrl() {
		assertEquals("",connection.call("null", "GET"));
	}
	
	@Test
	public void testTrueResponseUrl() {
		assertEquals("true",connection.call(URL_BOOL_TRUE, "GET"));
	}
	
	@Test
	public void testNumberUrl() {
		assertEquals("100",connection.call(URL_NUMBER, "GET"));
	}
	
	@Test
	public void testError400() {
		assertEquals("",connection.call(URL_ERROR_400, "GET"));
	}
	
	@Test
	public void testString() {
		assertEquals("Test",connection.call(URL_STRING, "GET"));
	}
}
