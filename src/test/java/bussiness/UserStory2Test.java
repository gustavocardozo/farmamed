package bussiness;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.User;

public class UserStory2Test {
	
	private RequestManager manager;
	private User u1, u2, u3;
	
	@Before
	public void instantiateObjects() {
		manager = new RequestManager();
		u1 = new User("User 1", "TestUser 1", 1234, true, true);
		u2 = new User("User 2", "TestUser 2", 1234, false, true);
		u3 = new User("User 3", "TestUser 3", 1234, true, false);
	}
	
	/*Caso 1:U2, U1 y U3 realizan solicitudes (en tiempos distintos y en el orden mencionado)
	 * CA: El primer elemento de la lista debe ser U1
	 * CA: El �ltimo elemento debe ser U2*/
	
	@Test
	public void testPriority1() {
		assertEquals(0, manager.getWaitingRequests().size());
		manager.addWaitingRequest(manager.createRequest(u2, null));
		manager.addWaitingRequest(manager.createRequest(u1, null));
		manager.addWaitingRequest(manager.createRequest(u3, null));
		assertEquals(3, manager.getWaitingRequests().size());
		assertEquals(u1, manager.getWaitingRequests().get(0).getUser());
		assertEquals(u2, manager.getWaitingRequests().get(2).getUser());
	}
	
	/*
	 * Caso 2: U2 y U3 realizan solicitudes. (en tiempos distintos y en el orden mencionado)
	 * CA: El primer elemento de la cola de la solicitud de R1 debe ser debe ser U3
	 * CA: El �ltimo elemento de la cola de la solicitud de R1 debe ser U2
	 */
	
	@Test
	public void testPriority2() {
		manager.addWaitingRequest(manager.createRequest(u2, null));
		manager.addWaitingRequest(manager.createRequest(u3, null));
		assertEquals(u3, manager.getWaitingRequests().get(0).getUser());
		assertEquals(u2, manager.getWaitingRequests().get(1).getUser());
	}
}
