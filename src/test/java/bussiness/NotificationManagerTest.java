package bussiness;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import ObjectTest.NotifierObjectTest;
import bussiness.NotificationManager;
import model.Notification;
import model.User;

public class NotificationManagerTest {

	private NotificationManager manager;
	private User user;
	private Notification notification;
	
	@Before
	public void instantiateObjects() {
		manager = new NotificationManager();
		user = new User("UserNameA", "UserSurnameA", 12, false, false);
		notification = new Notification("notify N");
	}
	
	@Test
	public void testGetUserNotificatios() {
		assertEquals(0,manager.getNotificationsByUser(user).size());
		manager.associateNotificationWithUser(user, notification);
		assertNotNull(manager.getNotificationsByUser(user));
		assertEquals(notification, manager.getNotificationsByUser(user).get(0));
	}
	
	@Test
	public void testAssociateNotificationWithUser1() {
		ArrayList<Notification> notifications = manager.getNotificationsByUser(user);
		assertEquals(0,notifications.size());
	}
	
	@Test
	public void testAssociateNotificationWithUser2() {
		manager.associateNotificationWithUser(user, notification);
		ArrayList<Notification> notifications = manager.getNotificationsByUser(user);
		assertEquals(notification, notifications.get(0));
		assertEquals(notification.getNotificationMessage(), notifications.get(0).getNotificationMessage());
	}
	
	@Test
	public void testAssociateNotificationWithUser3() {
		manager.associateNotificationWithUser(user, notification);
		ArrayList<Notification> notifications = manager.getNotificationsByUser(user);
		int currentNotifications = notifications.size();
		assertEquals(currentNotifications, notifications.size());
		Notification notificationAux = new Notification("notify Aux");
		manager.associateNotificationWithUser(user, notificationAux);
		notifications = manager.getNotificationsByUser(user);
		assertNotEquals(currentNotifications, notifications.size());
		assertTrue(currentNotifications < notifications.size());
	}
	
	@Test
	public void testNotificationReceived() {
		assertFalse(notification.getStatus().isReceived());
		manager.notificationReceived(notification.getStatus());
		assertFalse(!notification.getStatus().isReceived());
		assertTrue(notification.getStatus().isReceived());
	}
	
	@Test
	public void testNotificationRead() {
		assertFalse(notification.getStatus().isRead());
		manager.notificationRead(notification.getStatus());
		assertFalse(!notification.getStatus().isRead());
		assertTrue(notification.getStatus().isRead());
	}
	
	@Test
	public void testNotificationClean() {
		manager.associateNotificationWithUser(user, notification);
		assertEquals(1, manager.getNotificationsByUser(user).size());
		assertNotEquals(0, manager.getNotificationsByUser(user).size());
		manager.notificationClean(user, notification);
		assertEquals(0, manager.getNotificationsByUser(user).size());
		assertNotEquals(1, manager.getNotificationsByUser(user).size());
	}
	
	@Test
	public void testNotify() {
		NotifierObjectTest observer = new NotifierObjectTest();
		manager.addObserver(observer);
		manager.notifyUser(user, notification);
		assertEquals(notification, observer.getN());
	}
}
