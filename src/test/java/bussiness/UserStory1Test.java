package bussiness;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.Before;
import org.junit.Test;
import ObjectTest.NotifierObjectTest;
import model.Notification;
import model.User;

public class UserStory1Test {
	
	private NotificationManager manager;
	private User user;
	private Notification notification;
	private NotifierObjectTest observerTest;
	
	@Before
	public void instantiateObjects() {
		manager = new NotificationManager();
		observerTest = new NotifierObjectTest();
		manager.addObserver(observerTest);
		observerTest.setNotificationManager(manager);
		user = new User("User 1", "User 1 test", 12, false, false);
		notification = new Notification("El remedio M1 se encuentra en la farmacia F1");
	}
	
	/*CA: Una vez notificado, el listado de notificaciones que ten�a 0 (cero) elementos 
	 * ahora debe tener 1 (uno).*/
	@Test
	public void notifyTest1() {
		assertEquals(0, manager.getNotificationsByUser(user).size());
		manager.notifyUser(user, notification);
		assertEquals(1, manager.getNotificationsByUser(user).size());
	}
	
	/*CA: Verificar que en el listado de notificaciones enviadas a U1 (user 1) exista una notificaci�n 
	 * con el medicamento M1 en la farmacia F1
	 *CA: Chequear que no exista la notificaci�n con informaci�n de F2.*/
	@Test
	public void notifyTest2() {
		manager.notifyUser(user, notification);
		Notification notificationSent = manager.getNotificationsByUser(user).get(0);
		assertTrue(isContainedIn("M1", notificationSent.getNotificationMessage()));
		assertTrue(isContainedIn("F1", notificationSent.getNotificationMessage()));
		assertFalse(isContainedIn("F2", notificationSent.getNotificationMessage()));
	}
	
	/*CA: Si U1 recibe N1, el estado de la misma debe cambiar (recibido = false ---> recibido = true).
	 */
	@Test
	public void notifyTest3() {
		assertFalse(notification.getStatus().isReceived());
		manager.notifyUser(user, notification);
		assertTrue(notification.getStatus().isReceived());
	}
	
	/*CA: U1 lee a N1, el estado de esta cambia (le�do = false ---> le�do = true)
	 *CA: El listado de notificaciones que ten�a una notificaci�n (N1) 
	 *luego de le�da debe tener cero notificaciones. (borrado autom�tico de notificaciones le�das)*/
	@Test
	public void notifyTest4() {
		manager.notifyUser(user, notification);
		assertFalse(notification.getStatus().isRead());
		assertEquals(1, manager.getNotificationsByUser(user).size());
		observerTest.simulateReading(user, 0);
		assertTrue(notification.getStatus().isRead());
		assertEquals(0, manager.getNotificationsByUser(user).size());
	}
	
	private boolean isContainedIn(String s1, String s2) {
		Matcher m = getMatcher(s1, s2);
		return m.find();
	}

	private  Matcher getMatcher(String s1, String s2) {
		Pattern pattern = Pattern.compile(s1);
		return pattern.matcher(s2);
	}
}
