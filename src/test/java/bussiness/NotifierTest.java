package bussiness;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import ObjectTest.NotifierObjectTest;
import model.Notification;

public class NotifierTest {
	
	private Notifier notifier;
	private Notification n1;
	private NotifierObjectTest objectTest1;
	private NotificationManager manager;
	
	@Before
	public void instantiateObjects() {
		manager = new NotificationManager();
		notifier = new Notifier(manager);
		n1 = new Notification("notify N1");
		objectTest1 = new NotifierObjectTest();
	}
	
	@Test
	public void testAddObserver() {
		assertEquals(0,notifier.countObservers());
		assertNotEquals(2,notifier.countObservers());
		notifier.addObserver(objectTest1);
		assertEquals(1,notifier.countObservers());
		assertNotEquals(0,notifier.countObservers());
	}
	
	@Test
	public void testDeleteObserver() {
		notifier.addObserver(objectTest1);
		assertEquals(1,notifier.countObservers());
		assertNotEquals(0,notifier.countObservers());
		notifier.deleteObserver(objectTest1);
		assertEquals(0,notifier.countObservers());
		assertNotEquals(1,notifier.countObservers());
	}
	
	@Test
	public void testNotifyObservers() {
		notifier.notifyObservers();
	}

	@Test
	public void testNotify() {
		assertNull(objectTest1.getN());
		notifier.addObserver(objectTest1);
		notifier.notify(n1);
		assertEquals(n1,objectTest1.getN());
	}
	
	@Test
	public void testGetNotificationManager() {
		NotificationManager managerAux = new NotificationManager();
		assertEquals(manager, notifier.getNotificationManager());
		assertNotEquals(managerAux, notifier.getNotificationManager());
	}
}


