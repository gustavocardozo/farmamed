package bussiness;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;

import model.RequestStatus;

public class RequestStateModifierTest {
	
	private RequestStatus status;
	private RequestStateModifier stateModifier;
	
	@Before
	public void instantiateObjects() {
		status = new RequestStatus();
		stateModifier = new RequestStateModifier();
	}
	
//	public void testRequestPerformed() {
//		assertFalse(status.isSearchPerformed());
//		stateModifier.requestPerformed(status);
//		assertTrue(status.isSearchPerformed());
//		assertFalse(!status.isSearchPerformed());
//	}
//	
//	public void testRequestSuccessful() {
//		assertFalse(status.isSuccessfulSearch());
//		stateModifier.requestSuccessful(status);
//		assertTrue(status.isSuccessfulSearch());
//		assertFalse(!status.isSuccessfulSearch());
//	}
	
	public void testWaitingRequest() {
		assertFalse(status.isWaiting());
		stateModifier.waitingRequest(status);
		assertTrue(status.isWaiting());
		assertFalse(!status.isWaiting());
	}

}
