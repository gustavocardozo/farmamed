package comunication;


public class PamiCodeGateway {
	private PamiCodeCircuit circuit;
	
	public PamiCodeGateway (PamiCodeCircuit cir) {
		circuit = cir;
	}

	public boolean validate(String code) {
		return	circuit.validate(code);
	}
}
