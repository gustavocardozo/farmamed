package comunication;

public class PamiCodeCircuit {

	public enum Status {
		CLOSED,
		OPEN
	}
	
	private int count;
	private int autoResponseCount;
	private int maxErrors = 2;
	private int maxAutoResponse = 2;
	private Status status;
	private PamiCodeSupplier service;
	
	public PamiCodeCircuit(PamiCodeSupplier serv, Status stat) {
		status = stat;
		service = serv;
		restartCounters();
	}
	
	public Status getStatus() {
		return status;
	}
	
	private void restartCounters() {
		count = 0;
		autoResponseCount = 0;
	}
	
	private boolean isClose () {
		return status == Status.CLOSED;
	}
	
	private void setStatusCircuit(Status handle) {
		status = handle;
	}
	
	private void incrementError () {
		count++;
		if (count >= maxErrors) {
			setStatusCircuit(Status.OPEN);
		}
	}
	
	private void incrementAutoResponse () {
		autoResponseCount++;
		if (autoResponseCount >= maxAutoResponse) {
			setStatusCircuit(Status.CLOSED);
		}
	}
	
	public boolean validate (String code) {
		if (isClose()) {
			boolean response = service.validate(code);
			if (service.getError() != "") {
				incrementError();
			}
			return response;
		} else {
			incrementAutoResponse();
			return false;
		}
	}
}
