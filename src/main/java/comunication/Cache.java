package comunication;

import java.util.HashMap;

public class Cache<X, Y> {
    private HashMap<X, Y> list = new HashMap<X, Y>();

    public boolean contains (X object) {
        return list.containsKey(object);
    }

    public Y get(X object) {
        return list.get(object);
    }

    public Y put(X object, Y value) {
        return list.put(object, value);
    }
}
