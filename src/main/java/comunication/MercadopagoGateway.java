package comunication;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import bussiness.ConnectionManager;
import model.RemedyArticle;
import model.User;
import transformers.MPPaymentMethodTransformer;
import transformers.MPPaymentTransformer;
import validators.MPPaymentMethodsResponseValidator;
import validators.MPPaymentsResponseValidator;

public class MercadopagoGateway {
	private String ACCESS_TOKEN;
	public String URL_NEW_PAYMENT = "https://api.mercadopago.com/v1/payments";
	public String URL_PAYMENT_METHODS = "https://api.mercadopago.com/v1/payment_methods";
	public String METHOD = "GET";
	private MPPaymentMethodsResponseValidator validatorPaymentMethod = new MPPaymentMethodsResponseValidator();
	private MPPaymentsResponseValidator validatorPayments = new MPPaymentsResponseValidator();
	private ConnectionManager conection = new ConnectionManager();
	private String error;
	
	public MercadopagoGateway(String accessToken) {
		ACCESS_TOKEN = accessToken;
		cleanErrors();
	}
	
	private void cleanErrors () {
		error = "";
	}
	
	private String getAuthentication() {
		return "access_token=" + ACCESS_TOKEN;
	}
	
	public List<MPPaymentMethodDTO> getPaymentMethodCash() {
		List<MPPaymentMethodDTO> list = new ArrayList<MPPaymentMethodDTO>();
		String response;
		
		response = conection.call(URL_PAYMENT_METHODS + "?" +  getAuthentication(), METHOD);
		
		if (! validatorPaymentMethod.validate(response)) {
			error = response;
			return list;
		}
		list = MPPaymentMethodTransformer.transform(response);
		List<MPPaymentMethodDTO> cashList = new ArrayList<MPPaymentMethodDTO>();
		for (MPPaymentMethodDTO method : list) {
			if (method.getPaymentTypeId().contains("ticket")) {
				cashList.add(method);
			}
		}
		
		return cashList; // only ticket payment method
	}
	
	private JSONObject buildPayment(User user, RemedyArticle article, MPPaymentMethodDTO method) {
		JSONObject payment = new JSONObject();
		JSONObject payer   = new JSONObject();
		JSONObject aditional   = new JSONObject();
		JSONObject item   = new JSONObject();

		payment.put("description", "Compra de medicamento");
		payment.put("transaction_amount", article.getPrice());
		payment.put("installments", 1);// Obligatory in MP
		payment.put("payment_method_id", method.getId());
		
		payer.put("email",user.getEmail());
		payer.put("first_name", user.getName());
		payer.put("last_name", user.getSurname());
		
		item.put("title", article.getRemedy().getDrug());
		item.put("unit_price", article.getPrice());
		item.put("quantity", 1);
		List<JSONObject> items = new ArrayList<JSONObject>();
		items.add(item);
		
		aditional.put("items", items);
		
		payment.put("payer", payer);
		payment.put("additional_info", aditional);
		
		return payment;
	}
	
	public MPPaymentDTO buyRemedy(User user, RemedyArticle article, MPPaymentMethodDTO method) {
		
		// Crear ej json
		JSONObject request = buildPayment(user, article , method);
		
		String response;
		// Crear el pago (MP)
		response = conection.callJson(URL_NEW_PAYMENT + "?" +  getAuthentication() , "POST", request);
		
		if (! validatorPayments.validate(response)) {
			error = validatorPayments.getError();
			return null;
		}
		
		// Translate de respuesta
		return MPPaymentTransformer.transform(response, method);
	}
	
	public void setConection(String url, String method) {
		URL_NEW_PAYMENT = url;
		METHOD = method;
	}
	
	public String getError() {
		return error + conection.getError();
	}
}
