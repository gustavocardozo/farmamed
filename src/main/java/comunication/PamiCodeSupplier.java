package comunication;

import org.json.JSONObject;

import bussiness.*;


public class PamiCodeSupplier {
	private String URL_PAMI = "http://www.mocky.io/v2/5cb54968330000da0c5d7a18";
	private String METHOD = "GET";
	private ConnectionManager connection = new ConnectionManager();
	private String error;
	
	public PamiCodeSupplier (String url, String method) {
		URL_PAMI = url;
		METHOD = method;
		cleanErrors();
	}
	
	private void cleanErrors() {
		error = "";
	}

	public boolean validate(String code) {
		cleanErrors();
		String response = connection.call(URL_PAMI + "?code=" + code, METHOD);
		if (connection.getError() != "") {
			error = connection.getError();
			return false;
		}
		
		JSONObject obj = new JSONObject(response);
		String status = obj.get("success").toString();
		return  status == "true" ? true : false;
	}
	public String getError() {
		return error;
	}
}
