package comunication;

import bussiness.ConnectionManager;
import model.Address;
import model.Pharmacy;
import transformers.PharmacyAddressTransformer;
import validators.PharmacyAddressGatewayValidator;

import java.util.ArrayList;
import java.util.List;

public class PharmacyAddressGateway implements LoaderGateway<Pharmacy, Address> {
    // Implementar service para ir a buscar las farmacias
    protected String URL_PHARMACIES;
    protected String METHOD;

    protected Cache<Pharmacy, Address> cache = new Cache<Pharmacy, Address>();

    private PharmacyAddressGatewayValidator validator = new PharmacyAddressGatewayValidator();
    private ConnectionManager conection = new ConnectionManager();
    private String error;

    public PharmacyAddressGateway(String url, String method) {
        URL_PHARMACIES = url;
        METHOD = method;
        cleanErrors();
    }

    private void cleanErrors () {
        error = "";
    }

    public List<Address> findAll(Pharmacy pharmacy) {
       return new ArrayList<Address>();
    }

    public Address findOne(Pharmacy pharmacy) {
        //return new Address("Calle falsa", 1234, "Springfield");

        // Rebotar si es 105
        if (pharmacy.getId() == 2) { // TODO: realizar una api que sea dinámica
            return null;
        }
        // Preguntar si ya lo tengo (CACHE)
        if (cache.contains(pharmacy)) {
            return cache.get(pharmacy);
        }

        String response;
        try {
            response = conection.call(URL_PHARMACIES , METHOD);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            return null;
        }
        if (! validator.validate(response)) {
            error = response;
            return null;
        }
        Address address = PharmacyAddressTransformer.transform(response);
        cache.put(pharmacy, address);
        return address;
    }
}
