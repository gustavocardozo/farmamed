package comunication;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import bussiness.ConnectionManager;
import model.Pharmacy;
import model.Remedy;
import transformers.PharmacyTransformer;
import validators.PharmacyGatewayValidator;

public class PharmacyGateway {
	public static String URL_MEDICAMENTS = "http://www.mocky.io/v2/5cb538e4330000eb0c5d79e7";
	public static String METHOD = "GET";
	private PharmacyGatewayValidator validator = new PharmacyGatewayValidator();
	private ConnectionManager conection = new ConnectionManager();
	private String error;

	public PharmacyGateway() {
		URL_MEDICAMENTS = "http://www.mocky.io/v2/5cb538e4330000eb0c5d79e7";
		METHOD = "GET";
		cleanErrors();
	}
	
	public PharmacyGateway(String url, String method) {
		URL_MEDICAMENTS = url;
		METHOD = method;
		cleanErrors();
	}
	
	private void cleanErrors () {
		error = "";
	}
	
	public List<Pharmacy> getByRemedy(Remedy remedy) {
		List<Pharmacy> list = new ArrayList<Pharmacy>();
		String response;
		try {
			response = conection.call(URL_MEDICAMENTS + "/" + URLEncoder.encode(remedy.getDrug(), "UTF-8"), METHOD);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			return list;
		}
		if (! validator.validate(response)) {
			error = response;
			return list;
		}
		return PharmacyTransformer.transform(response);
	}
	
	public void setConection(String url, String method) {
		URL_MEDICAMENTS = url;
		METHOD = method;
	}
	
	public String getError() {
		return error + conection.getError();
	}
}
