package comunication;

public class MPPaymentDTO {
	private int id;
	private String status;
	private String statusDetail;
	private String externalResourceUrl;
	
	private MPPaymentMethodDTO paymentMethod;
	
	public MPPaymentDTO(int id, String status, String statusDetail, String externalResourceUrl, MPPaymentMethodDTO method) {
		this.id = id;
		this.status = status;
		this.statusDetail = statusDetail;
		this.externalResourceUrl = externalResourceUrl;
		this.paymentMethod = method;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusDetail() {
		return statusDetail;
	}

	public void setStatusDetail(String statusDetail) {
		this.statusDetail = statusDetail;
	}

	public String getExternalResourceUrl() {
		return externalResourceUrl;
	}

	public void setExternalResourceUrl(String externalResourceUrl) {
		this.externalResourceUrl = externalResourceUrl;
	}

	public MPPaymentMethodDTO getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(MPPaymentMethodDTO paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	
}
