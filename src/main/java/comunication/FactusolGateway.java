package comunication;

import org.json.JSONObject;

import bussiness.ConnectionManager;

public class FactusolGateway {
	public String URL_NEW_PAYMENT = "http://www.mocky.io/v2/5ce81fbb350000ba40cf63e2";
	public String METHOD = "GET";
	private ConnectionManager conection = new ConnectionManager();
	private String error;

	public FactusolGateway() {
		cleanErrors();
	}
	
	public FactusolGateway(String url, String method) {
		URL_NEW_PAYMENT = url;
		METHOD = method;
		cleanErrors();
	}
	
	private void cleanErrors () {
		error = "";
	}
	
	public boolean informPayment(JSONObject payment) {
		String response;
		response = conection.callJson(URL_NEW_PAYMENT , METHOD, payment);
		if (response == "") {
			return false;
		}
		return true;
	}
	
	public void setConection(String url, String method) {
		URL_NEW_PAYMENT = url;
		METHOD = method;
	}
	
	public String getError() {
		return error + conection.getError();
	}
}
