package comunication;

import java.util.List;

public interface LoaderGateway<T, U> {
    public List<U> findAll(T object);
    public U findOne(T object);
}
