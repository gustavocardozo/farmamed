package comunication;

public class MPPaymentMethodDTO {
	private String id;
	private String name;
	private String paymentTypeId;
	
	public MPPaymentMethodDTO(String id, String name, String paymentTypeId) {
		this.id = id;
		this.name = name;
		this.paymentTypeId = paymentTypeId;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPaymentTypeId() {
		return paymentTypeId;
	}
	public void setPaymentTypeId(String paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}
	
	
}
