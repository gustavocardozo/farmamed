package Main;

import java.util.Observable;
import java.util.Observer;

import bussiness.NotificationManager;
import bussiness.Notifier;
import model.Notification;

public class NotifierObjectTest implements Observer{

	private Notification n;
	@Override
	public void update(Observable o, Object arg) {
		n = (Notification) arg;
		Notifier not = (Notifier) o;
		not.getNotificationManager().notificationReceived(n.getStatus());
//		NotificationManager.notificationReceived(n.getStatus());
		System.out.println(n.getNotificationMessage());
	}
	public Notification getN() {
		return n;
	}

}
