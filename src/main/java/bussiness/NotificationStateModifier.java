package bussiness;

import model.NotificationStatus;

public class NotificationStateModifier {
	
	public void notificationReceived(NotificationStatus status) {
		status.setReceived(true);
	}
	
	public void notificationRead(NotificationStatus status) {
		status.setRead(true);
	}

}
