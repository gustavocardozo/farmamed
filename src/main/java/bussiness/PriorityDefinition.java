package bussiness;

import model.User;

public class PriorityDefinition {

	
	public PriorityDefinition() {
	}
	

	public int definePriority(User u) {
		return calculatePriority(u);
	}

	private int calculatePriority(User u) {
		int priorityLevel = 0;
		if(u.isDiabetic()) {priorityLevel += 2;}
		if(u.isOncological()) {priorityLevel += 1;}
		return priorityLevel;
	}
	
}
