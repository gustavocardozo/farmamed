package bussiness;

import model.Notification;
import model.User;

public class NotificationsCleaner {
	
	public void cleanRead(User u, Notification n) {
		u.getNotifications().remove(n);
	}
}
