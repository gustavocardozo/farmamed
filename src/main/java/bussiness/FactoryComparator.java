package bussiness;

import java.util.Comparator;

import model.Request;

public class FactoryComparator {

	private FactoryComparator() {
	}

	public static Comparator<Request> higherPriority()
	{
		return (request1, request2) -> (int) (request2.getPriority() - request1.getPriority());
	}

}
