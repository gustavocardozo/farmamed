package bussiness;

import java.util.Observer;

import model.Notification;
import model.SubjectObservable;

public class Notifier extends SubjectObservable{
	
	private NotificationManager manager;
	
	public Notifier(NotificationManager manager) {
		super();
		this.manager = manager;
	}
	
	public void notify(Notification n) {
		setChanged();
		notifyObservers(n);
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}
	
	public NotificationManager getNotificationManager() {
		return manager;
	}

	@Override
	public void notifyObservers(Object arg0) {
		if(hasChanged()) {
			for (Observer observer : observers) {
				observer.update(this, arg0);
			}
			clearChanged();
		}
	}
	
	public int countObservers() {
		return observers.size();
	}

}
