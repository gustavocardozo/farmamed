package bussiness;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

public class ConnectionManager {
	public static int timeout = 3000;
	private String error;
	
	public ConnectionManager() {
		cleanErrors();
	}
	
	private void cleanErrors () {
		error = "";
	}

	public String send(String path, String method) {
		try {
			cleanErrors();
			URL url = new URL(path);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod(method);
			conn.setConnectTimeout(timeout);
			conn.setRequestProperty("Accept", "application/json");
	
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}
	
			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
			String output;
			String response = "";
			while ((output = br.readLine()) != null) {
				response = response + output;
			}
			conn.disconnect();
			
			return response;
		} catch (Exception e) {
			// TODO: handle exception
			error = e.getMessage();
			return "";
		}
	}
	
	public String callJson(String path, String method, JSONObject json) {
		try {
			cleanErrors();
			URL url = new URL(path);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod(method);
			conn.setConnectTimeout(timeout);
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Content-Type", "application/json; utf-8");
			conn.setDoOutput(true);

			OutputStream os = conn.getOutputStream();
			os.write(json.toString().getBytes("UTF-8"));
			os.close();
			 
			int httpResult = conn.getResponseCode();
	
			InputStream is = null;
			if (httpResult >= 200 && httpResult < 400) {
			   // Create an InputStream in order to extract the response object
			   is = conn.getInputStream();
			}
			else {
			   is = conn.getErrorStream();
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String output;
			String response = "";
			while ((output = br.readLine()) != null) {
				response = response + output;
			}
			conn.disconnect();
			
			return response;
		} catch (Exception e) {
			// TODO: handle exception
			error = e.getMessage();
			return "";
		}
	}
	
	public String call(String path, String method) {
		return this.send(path, method);
	}
	
	public void setTimeout(int time) {
		timeout = 1;
	}
	
	public String getError() {
		return error;
	}
}
