package bussiness;

import java.util.ArrayList;
import java.util.Observer;

import model.Notification;
import model.NotificationStatus;
import model.User;

public class NotificationManager {
	
	private MapperNotificationUser mapperNotificationUser;
	private NotificationsCleaner notificationsCleaner;
	private NotificationStateModifier notificationStateModifier;
	private Notifier notifier;
	
	public NotificationManager() {
		mapperNotificationUser = new MapperNotificationUser();
		notificationsCleaner = new NotificationsCleaner();
		notificationStateModifier = new NotificationStateModifier();
		notifier = new Notifier(this);
	}
	
	public void notifyUser(User u, Notification n) {
		associateNotificationWithUser(u, n);
		notify(n);
	}
	
	public void associateNotificationWithUser (User u, Notification n) {
		mapperNotificationUser.associateNotificationWithUser(u, n);
	}
	
	public void notificationReceived(NotificationStatus status) {
		notificationStateModifier.notificationReceived(status);
	}
	
	public void notificationRead(NotificationStatus status) {
		notificationStateModifier.notificationRead(status);
	}
	
	public void notificationClean(User u, Notification n) {
		notificationsCleaner.cleanRead(u, n);
	}
	
	public void notify(Notification n) {
		notifier.notify(n);
	}
	
	public void addObserver(Observer o) {
		notifier.addObserver(o);
	}
	
	public ArrayList<Notification> getNotificationsByUser(User user){
		return mapperNotificationUser.getUserNotifications(user);
	}
}
