package bussiness;

import java.util.ArrayList;

import model.Recipe;
import model.Request;
import model.User;

public class RequestManager {
	
	private PriorityDefinition priorityDefinition;
	private ArrayList<Request> waitingRequests;
	private RequestStateModifier requestStateModifier;
	
	/* 1) Se crea una solicitud ---> se busca el/los medicamentos ---> se setea el estado de la solicitud (searchPerformed = true)
	 * 2) Si se encuentra el medicamento ---> se setea el estado de la solicitud (successfulSearch = true)
	 * 3) Si no se encuentra ---> se pregunta si desea notificar cuando este disponible el medicamento ---> 
	 * resupuesta si ---> se setea el estado de la solicitud (waiting = true) ---> se guarda
	 * 4) Si no lo desea, la solicitud se ignora*/
	
	
	public RequestManager() {
		
		requestStateModifier = new RequestStateModifier();
		this.priorityDefinition = new PriorityDefinition();
		this.waitingRequests = new ArrayList<Request>();
	}

	//1
	public Request createRequest(User user, Recipe recipe) {
		Request request = new Request(user, recipe);
		request.setPriority(priorityDefinition.definePriority(user));
		return request;
	}
	
	//3
	public void addWaitingRequest(Request request) { 
		requestStateModifier.waitingRequest(request.getStatus());
		waitingRequests.add(request);
	}
	
	public ArrayList<Request> getWaitingRequests(){
		waitingRequests.sort(FactoryComparator.higherPriority());
		return waitingRequests;
	}

	public RequestStateModifier getRequestStateModifier() {
		return requestStateModifier;
	}
}
