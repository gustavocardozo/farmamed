package bussiness;

import java.util.ArrayList;
import model.Notification;
import model.User;

public class MapperNotificationUser {
	
	public MapperNotificationUser() { 
	}
	
	public void associateNotificationWithUser (User u, Notification n) {
		u.getNotifications().add(n);	
	}
	
	public ArrayList<Notification> getUserNotifications(User u) {
		return u.getNotifications();
	}
}
