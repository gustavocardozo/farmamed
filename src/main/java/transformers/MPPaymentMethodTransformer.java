package transformers;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import comunication.MPPaymentMethodDTO;

public class MPPaymentMethodTransformer {

	public static List<MPPaymentMethodDTO> transform(String response) {
		List<MPPaymentMethodDTO> list = new ArrayList<MPPaymentMethodDTO>();
		JSONArray arr = new JSONArray(response);
        for (int i = 0; i < arr.length(); i++) {
            JSONObject obj = arr.getJSONObject(i);
        	String id = obj.getString("id");
        	String name = obj.getString("name");
        	String paymentTypeId = obj.getString("payment_type_id");
            list.add(new MPPaymentMethodDTO(id, name, paymentTypeId));
        }
        return list;
	}
}
