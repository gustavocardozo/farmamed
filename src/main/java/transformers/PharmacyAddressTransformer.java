package transformers;

import model.Address;
import model.Pharmacy;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PharmacyAddressTransformer {

	public static Address transform(String response) {
		JSONObject obj = new JSONObject(response);
        String street = obj.getString("street");
        int number = obj.getInt("number");
        String city = obj.getString("city");
        return new Address(street, number, city);
	}
}
