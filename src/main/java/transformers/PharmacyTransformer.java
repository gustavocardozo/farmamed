package transformers;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import model.Address;
import model.Pharmacy;

public class PharmacyTransformer {

	public static List<Pharmacy> transform(String response) {
		List<Pharmacy> list = new ArrayList<Pharmacy>();
		JSONObject obj = new JSONObject(response);
		JSONArray arr = obj.getJSONArray("pharmacies");
        for (int i = 0; i < arr.length(); i++) {
            String name = arr.getJSONObject(i).getString("name");
            String dir = arr.getJSONObject(i).getString("address");
            String phone = arr.getJSONObject(i).getString("phone");
            Address address = new Address(dir, 0, "");
            list.add(new Pharmacy(name, address, phone));
        }
        return list;
	}
}
