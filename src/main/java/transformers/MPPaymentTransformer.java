package transformers;

import org.json.JSONObject;

import comunication.MPPaymentDTO;
import comunication.MPPaymentMethodDTO;

public class MPPaymentTransformer {

	public static MPPaymentDTO transform(String response, MPPaymentMethodDTO method) {
		JSONObject obj = new JSONObject(response);
		JSONObject transactionDetails = obj.getJSONObject("transaction_details");
       
    	int id = obj.getInt("id");
    	String status = obj.getString("status");
    	String statusDetail = obj.getString("status_detail");
    	String externalResourceUrl = transactionDetails.getString("external_resource_url");
		
        return new MPPaymentDTO(id, status, statusDetail, externalResourceUrl, method);
	}
}
