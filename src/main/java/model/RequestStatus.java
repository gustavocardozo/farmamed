package model;

public class RequestStatus {
	
	private boolean searchPerformed, successfulSearch, waiting;
	
	public RequestStatus() {
	}

	public boolean isSearchPerformed() {
		return searchPerformed;
	}

	public void setSearchPerformed(boolean searchPerformed) {
		this.searchPerformed = searchPerformed;
	}

	public boolean isSuccessfulSearch() {
		return successfulSearch;
	}

	public void setSuccessfulSearch(boolean successfulSearch) {
		this.successfulSearch = successfulSearch;
	}

	public boolean isWaiting() {
		return waiting;
	}

	public void setWaiting(boolean waiting) {
		this.waiting = waiting;
	}
}
