package model;

public class Remedy {
	
	private String drug;
	private String trademark;
	private String version;

	public Remedy(String drug, String trademark, String version) {
		this.drug = drug;
		this.trademark = trademark;
		this.version = version;
	}

	public String getDrug() {
		return drug;
	}

	public void setDrug(String drug) {
		this.drug = drug;
	}

	public String getTrademark() {
		return trademark;
	}

	public void setTrademark(String trademark) {
		this.trademark = trademark;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
