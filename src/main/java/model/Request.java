package model;

import java.time.LocalDate;

public class Request {
	
	private User user;
	private Recipe recipe;
	private LocalDate date;
	private RequestStatus status = new RequestStatus();
	private int priority;

	public Request(User user, Recipe recipe) {
		this.user = user;
		this.recipe = recipe;
		date = LocalDate.now();
		priority = 0;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Recipe getRecipe() {
		return recipe;
	}

	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}

	public RequestStatus getStatus() {
		return status;
	}

	public LocalDate getDate() {
		return date;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
}
