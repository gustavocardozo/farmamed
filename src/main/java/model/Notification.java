package model;

public class Notification {
	
	private String notificationMessage;
	private NotificationStatus status = new NotificationStatus();
	
	public Notification(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}

	public String getNotificationMessage() {
		return notificationMessage;
	}

	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}

	public NotificationStatus getStatus() {
		return status;
	}
}
