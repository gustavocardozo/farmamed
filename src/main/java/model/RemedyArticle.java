package model;

public class RemedyArticle {
	private Remedy remedy;
	private float price;
	
	public RemedyArticle(Remedy remedy, float price) {
		this.remedy = remedy;
		this.price = price;
	}
	
	public Remedy getRemedy() {
		return remedy;
	}
	public void setRemedy(Remedy remedy) {
		this.remedy = remedy;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
}
