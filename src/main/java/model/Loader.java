package model;

import comunication.LoaderGateway;

import java.util.List;

public class Loader {
    protected Integer id;
    protected boolean loadFlag;

    private void initFlags() {
        loadFlag = false;
    }

    private boolean validateId() {
        return id == null || id == 0;
    }

    public Loader() {
        initFlags();
    }

    public boolean getLoadFlag() {
        return loadFlag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
