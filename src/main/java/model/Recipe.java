package model;

public class Recipe {
	
	private int recipeNumber;
	
	public Recipe(int recipeNumber) {
		this.recipeNumber = recipeNumber;
	}

	public int getRecipeNumber() {
		return recipeNumber;
	}

	public void setRecipeNumber(int recipeNumber) {
		this.recipeNumber = recipeNumber;
	}
	
}
