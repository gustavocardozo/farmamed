package model;

public class NotificationStatus {
	
	private boolean received;
	private boolean read;

	public NotificationStatus() {
	}

	public boolean isReceived() {
		return received;
	}

	public void setReceived(boolean received) {
		this.received = received;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}
}
