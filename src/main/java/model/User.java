package model;

import java.util.ArrayList;

public class User {
	
	private String name;
	private String surname;
	private int affiliateNumber;
	private boolean diabetic, oncological;
	private ArrayList<Notification> notifications = new ArrayList<Notification>();
	private String email;
	
	public User(String name, String surname, int affiliateNumber, boolean diabetic, boolean oncological) {
		this.name = name;
		this.surname = surname;
		this.affiliateNumber = affiliateNumber;
		this.diabetic = diabetic;
		this.oncological = oncological;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getAffiliateNumber() {
		return affiliateNumber;
	}

	public void setAffiliateNumber(int affiliateNumber) {
		this.affiliateNumber = affiliateNumber;
	}

	public ArrayList<Notification> getNotifications() {
		return notifications;
	}

	public boolean isDiabetic() {
		return diabetic;
	}

	public void setDiabetic(boolean diabetic) {
		this.diabetic = diabetic;
	}

	public boolean isOncological() {
		return oncological;
	}

	public void setOncological(boolean oncological) {
		this.oncological = oncological;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
