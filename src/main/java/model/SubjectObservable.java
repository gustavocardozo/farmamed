package model;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public abstract class SubjectObservable extends Observable{
	protected ArrayList<Observer> observers = new ArrayList<Observer>();
	
	@Override
	public void addObserver(Observer arg0) {
		observers.add(arg0);
	}
	
	@Override
	public int countObservers() {
		return observers.size();
	}
	
	@Override
	public void deleteObserver(Observer arg0) {
		observers.remove(arg0);
	}
	
	@Override
	public void deleteObservers() {
		observers.clear();
	}
	
	@Override
	protected void clearChanged() {
		super.clearChanged();
	}

	@Override
	public boolean hasChanged() {
		return super.hasChanged();
	}

	@Override
	protected void setChanged() {
		super.setChanged();
	}

	@Override
	public void notifyObservers() {
		if(hasChanged()) {
			for (Observer observer : observers) {
				observer.update(null, null);
			}
			clearChanged();
		}
	}
	
	@Override
	public void notifyObservers(Object arg0) {
		if(hasChanged()) {
			for (Observer observer : observers) {
				observer.update(null, arg0);
			}
			clearChanged();
		}
	}
}
