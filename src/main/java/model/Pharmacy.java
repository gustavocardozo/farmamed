package model;

import comunication.LoaderGateway;

import java.util.ArrayList;
import java.util.List;

public class Pharmacy extends Loader {
	private String name;
	private Address address;
	private String phone;

	private LoaderGateway<Pharmacy, Address> addressSource;

	public Pharmacy(String name, Address address, String phone) {
		this.name = name;
		this.address = address;
		this.phone = phone;
	}

	/**
	 * Construct for lazy load for a database (Api Rest)
	 * @param id
	 * @param addressSource
	 * @param name
	 * @param address
	 * @param phone
	 */
	public Pharmacy(int id, LoaderGateway<Pharmacy, Address> addressSource, String name, Address address, String phone) {
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.addressSource = addressSource;

		// Super
		this.id = id;
	}

	private Address loadAddress() {
		if (address == null ){
			if (!this.loadFlag) {
				Address aux = this.addressSource.findOne(this);
				if (aux != null) {
					this.loadFlag = true;
				}
				this.address = aux;
			}
			// TODO: Si está en true y en false? (Fijarse si se actualiza)
		}
		return address;// Only for wait cache
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return loadAddress();
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getTel() {
		return phone;
	}

	public void setTel(String tel) {
		this.phone = tel;
	}
}
