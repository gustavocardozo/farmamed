package validators;

import java.util.ArrayList;
import java.util.Arrays;

import org.json.JSONException;
import org.json.JSONObject;

public class MPPaymentsResponseValidator{
	ArrayList<Integer> httpErrors = new ArrayList<Integer>(Arrays.asList(400, 403, 404));
	String error = "";
	
	public boolean validate(String response) {
		error = "";
		try {
			JSONObject obj = new JSONObject(response);
			if (!obj.isNull("status")) {
				// It's integer?
				boolean integer = true;
				try {
					Integer.parseInt(obj.getString("status"));
				} catch(NumberFormatException e) {
					integer = false;
				}
				if (integer) {
					if (httpErrors.contains(obj.getInt("status"))) {
						throw new JSONException(obj.getString("message"));
					}
				}
			}
			
			return true;
		} catch (JSONException e) {
			// TODO: handle exception
			error = e.getMessage();
			return false;
		}
	}
	
	public String getError() {
		return error;
	}
}
