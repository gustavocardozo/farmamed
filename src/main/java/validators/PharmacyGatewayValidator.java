package validators;

import org.json.JSONException;
import org.json.JSONObject;

public class PharmacyGatewayValidator{
	
	public boolean validate(String response) {
		try {
			JSONObject obj = new JSONObject(response);
			if (obj.isNull("pharmacies")) {
				throw new JSONException("Formato incorrecto: Listado de farmacias no encontrado");
			}
			return true;
		} catch (JSONException e) {
			// TODO: handle exception
			return false;
		}
		
	}
}
