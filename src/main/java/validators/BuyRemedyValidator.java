package validators;

import java.util.HashMap;

import model.Pharmacy;
import model.RemedyArticle;

public class BuyRemedyValidator {
	private HashMap<RemedyArticle, Pharmacy> definition;
	
	public BuyRemedyValidator(HashMap<RemedyArticle, Pharmacy> definition) {
		this.definition = definition;
	}
	
	public boolean validate(RemedyArticle article, Pharmacy pharmacy) {
		if (definition.containsKey(article)) {
			return definition.get(article) == pharmacy;
		}
		return false;
	}
}
