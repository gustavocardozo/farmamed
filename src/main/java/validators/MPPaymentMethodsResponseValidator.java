package validators;

import org.json.JSONException;
import org.json.JSONObject;

public class MPPaymentMethodsResponseValidator{
	
	public boolean validate(String response) {
		try {
			if (response.startsWith("{")) {
				JSONObject obj = new JSONObject(response);
				if (!obj.isNull("status") && obj.getInt("status") == 400 ) {
					throw new JSONException(obj.getString("message"));
				}
				return false;
			}
			return true;
		} catch (JSONException e) {
			// TODO: handle exception
			return false;
		}
		
	}
}
